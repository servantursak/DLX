library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.constants.all;

entity boolLogic is
	generic (N: integer := Nbit);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		com: in std_logic_vector(1 downto 0);
		S: out std_logic_vector(N-1 downto 0)	
	);
end boolLogic;

architecture Behavioral of boolLogic is

begin
	process(A,B,com)
	begin
		case com is
			when "00" => 
				S <= A and B;
			when "01" => 
				S <= A or B;
			when "10" => 
				S <= A xor B;
			when "11" => 
				S <= not B;
			when others => 
				S <= (others => '0');
		end case;
	end process;
end Behavioral;

