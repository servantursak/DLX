library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pentium4adder is
	generic(N: integer := 32; R: integer := 4);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		Ci: in std_logic;
		S: out std_logic_vector(N-1 downto 0);
		Co: out std_logic
	);
end pentium4adder;

architecture Structural of pentium4adder is
	component carryGen is
		generic (N: integer := 32; R: integer := 4);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			Ci: in std_logic;
			Co_0: out std_logic;
			C: out std_logic_vector(N/R -1 downto 0)
		);
	end component carryGen;
	
	component sumg is
		generic(N: integer := 32;
		R: integer := 4);
	port( 
		A: IN std_logic_vector(N-1 downto 0);
		B: IN std_logic_vector(N-1 downto 0);
		Ci: IN std_logic_vector(N/R-1 downto 0);
		Ci_0: IN std_logic;
		S: OUT std_logic_vector(N-1 downto 0);
		Co: OUT std_logic);
	end component sumg;
	
	signal cin: std_logic_vector(N/4-1 downto 0);
	signal Cfirst: std_logic;

begin
	cg: carryGen generic map (N,R) port map (A,B,Ci,Cfirst,cin);
	sg: sumg generic map (N,R) port map (A,B,cin,Cfirst,S,Co);	
end Structural;

