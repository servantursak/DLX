library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.utils.all;

entity reg_file is
	generic(reg_size: integer := 32);
	port(	enable: in std_logic;
		clk: in std_logic;
		rst: in std_logic;
		rd1_en: in std_logic;
		rd2_en: in std_logic;
		wr_en_1, wr_en_2: in std_logic;
		rd1_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		rd2_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		wr_addr:  in std_logic_vector(log2(reg_size)-1 downto 0);
		data_in_1:  in std_logic_vector(reg_size-1 downto 0);
		data_in_2:  in std_logic_vector(reg_size-1 downto 0);
		data_out_1: out std_logic_vector(reg_size-1 downto 0);
		data_out_2: out std_logic_vector(reg_size-1 downto 0));
end reg_file;

architecture bhv of reg_file is

type regtype is array (0 to reg_size-1) of std_logic_vector(reg_size-1 downto 0);
signal rf : regtype;

begin

process(enable, clk) is
begin
rf(0) <= (others => '0'); --R0 always zero

if (rst = '1') then 
	rf <= (others => (others => '0' )) ;
elsif (enable='1') then 
	if (rising_edge(clk) or falling_edge(clk)) then
		if clk = '0' then
			if(wr_en_1='1') then
				rf(to_integer(unsigned(wr_addr))) <= data_in_1;
			end if;
			if(wr_en_2='1') then
				rf(to_integer(unsigned(wr_addr))+1) <= data_in_2;
			end if;
		else
			if(rd1_en='1') then
				data_out_1 <= rf(to_integer(unsigned(rd1_addr)));
			else
				data_out_1 <= (others => '0');
			end if;

			if(rd2_en='1') then
				data_out_2 <= rf(to_integer(unsigned(rd2_addr)));
			else
				data_out_2 <= (others => '0');
			end if;
		end if;
	end if;
else
	data_out_1 <= (others => 'Z');
	data_out_2 <= (others => 'Z');
end if;
end process;

end bhv;



