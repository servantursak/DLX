library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.utils.all;

entity dlx is
	generic(I_size: integer := IR_SIZE;
		PC_size: integer := PC_SIZE;
		FORWARDING_ENABLED :     boolean := TRUE;
		BRANCH_DELAY_ENABLED:    boolean := TRUE);
	port(	clk: std_logic;
		rst: std_logic;
		--syn (imem)
		addr_imem: out std_logic_vector(PC_size-1 downto 0);
		d_out_imem: in std_logic_vector(IR_size-1 downto 0);

		--syn (dmem)
		rm_dmem,wm_dmem,en_dmem: out std_logic;
		addr_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW_dmem: out std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out_dmem: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0));
end dlx;

architecture Structural of dlx is
	
   component dlx_cu is
		generic (
			MICROCODE_MEM_SIZE :     integer := MICROCODE_MEM_SIZE;  -- Microcode Memory Size
			FUNC_SIZE          :     integer := FUNC_SIZE;  -- Func Field Size for R-Type Ops
			OP_CODE_SIZE       :     integer := OP_CODE_SIZE;  -- Op Code Size
			ALU_OPC_SIZE     :     integer := EXE_CONTR_SIZE;  -- ALU Op Code Word Size
			IR_SIZE            :     integer := IR_SIZE;  -- Instruction Register Size    
			CW_SIZE            :     integer := CW_SIZE;  -- Control Word Size
			DMEM_CW_SIZE       :     integer := DMEM_CW_SIZE;
		        FORWARDING_ENABLED :     boolean := FALSE;
		        BRANCH_DELAY_ENABLED:    boolean := FALSE); 
		port (
			Clk                : in  std_logic;  -- Clock
			Rst                : in  std_logic;  -- Reset:Active-Low
			sign			   : out  std_logic;	 -- to differentiate addi-addui
			IR_IN              : in  std_logic_vector(IR_SIZE - 1 downto 0);
			IR_LATCH_EN        : out std_logic;  -- Instruction Register Latch Enable
			NPC_LATCH_EN       : out std_logic;
			RegA_LATCH_EN      : out std_logic;  -- Register A Latch Enable
			RegB_LATCH_EN      : out std_logic;  -- Register B Latch Enable
			RegIMM_LATCH_EN    : out std_logic;  -- Immediate Register Latch Enable
		--	MUXA_SEL           : out std_logic;  -- MUX-A Sel
			MUXB_SEL           : out std_logic;  -- MUX-B Sel
			ALU_OUTREG_EN      : out std_logic;  -- ALU Output Register Enable
   			J_CODE             : out std_logic_vector(2 downto 0);	
			ALU_OPCODE         : out std_logic_vector(ALU_OPC_SIZE-1 downto 0);
			DRAM_EN            : out std_logic;  -- Data RAM Write Enable
			DRAM_RD_WRN        : out std_logic;  -- Data RAM Write Enable
			LMD_LATCH_EN       : out std_logic;  -- LMD Register Latch Enable
			PC_LATCH_EN        : out std_logic;  -- Program Counte Latch Enable
			DMEM_CW	       	   : out std_logic_vector(DMEM_CW_SIZE-1 downto 0); --dmemCW vector
			WB_MUX_SEL         : out std_logic;  -- Write Back MUX Sel
			RF_WE_1            : out std_logic;	-- Register File Write Enable 1
			RF_WE_2            : out std_logic;
			FW_EXE_MEMN_A      : out std_logic;
		        FW_EXE_MEMN_B      : out std_logic;
		        FW_EN_A	    	   : out std_logic;
		        FW_EN_B	    	   : out std_logic;
		        FW_EN_M	    	   : out std_logic;
			hold_sel: out std_logic;  
    			hold_en: out std_logic;
			FLUSH_EN: in std_logic
		);  -- Register File Write Enable 2
	end component dlx_cu;
	
	component datapath is
		generic(
			IR_SIZE            :     integer := I_SIZE;  -- Instruction Register Size  
			DMEM_CW_SIZE       :     integer := DMEM_CW_SIZE --dmemCW Size  
		);
		port(
			Clk                : in  std_logic;  -- Clock
			Rst                : in  std_logic;  -- Reset:Active-Low
			sign			   : in  std_logic;	 -- to differentiate addi-addui
			IR_LATCH_EN        : in std_logic;  -- Instruction Register Latch Enable
			NPC_LATCH_EN       : in std_logic; -- NextProgramCounter Register Latch Enable
			RegA_LATCH_EN      : in std_logic;  -- Register A Latch Enable
			RegB_LATCH_EN      : in std_logic;  -- Register B Latch Enable
			RegIMM_LATCH_EN    : in std_logic;  -- Immediate Register Latch Enable
			--MUXA_SEL           : in std_logic;  -- MUX-A Sel
			MUXB_SEL           : in std_logic;  -- MUX-B Sel
			ALU_OUTREG_EN      : in std_logic;  -- ALU Output Register Enable
   			J_CODE             : in std_logic_vector(2 downto 0);
			ALU_OPCODE         : in std_logic_vector(EXE_CONTR_SIZE-1 downto 0);
			DRAM_EN            : in std_logic;  -- Data RAM Write Enable
			DRAM_RD_WRN        : in std_logic;  -- Data RAM Write Enable
			LMD_LATCH_EN       : in std_logic;  -- LMD Register Latch Enable
			--PC_LATCH_EN        : in std_logic;  -- Program Counte Latch Enable
			DMEM_CW            : in std_logic_vector(DMEM_CW_SIZE-1 downto 0);  -- dmemCW vector
			WB_MUX_SEL         : in std_logic;  -- Write Back MUX Sel
			RF_WE_1            : in std_logic;  -- Register File Write 1 Enable
			RF_WE_2            : in std_logic;  -- Register File Write 2 Enable
			IR_OUT		   : out std_logic_vector(IR_SIZE-1 downto 0);
			FW_EXE_MEMN_A	    : in std_logic;
			FW_EXE_MEMN_B	    : in std_logic;
			FW_EN_A	      	    : in std_logic;
			FW_EN_B	       	    : in std_logic;
			FW_EN_M	       	    : in std_logic;
			hold_sel: in std_logic;  
    			hold_en: in std_logic;
			FLUSH_EN: out std_logic;
			--syn (imem)
		addr_imem: out std_logic_vector(PC_size-1 downto 0);
		d_out_imem: in std_logic_vector(IR_size-1 downto 0);

		--syn (dmem)
		rm_dmem,wm_dmem,en_dmem: out std_logic;
		addr_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW_dmem: out std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out_dmem: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0)
		);
	end component datapath;

	signal IR_LATCH_EN        : std_logic;  -- Instruction Register Latch Enable
	signal NPC_LATCH_EN       : std_logic; -- NextProgramCounter Register Latch Enable
	signal RegA_LATCH_EN      : std_logic;  -- Register A Latch Enable
	signal RegB_LATCH_EN      : std_logic;  -- Register B Latch Enable
	signal RegIMM_LATCH_EN    : std_logic;  -- Immediate Register Latch Enable
	signal MUXA_SEL           : std_logic;  -- MUX-A Sel
	signal MUXB_SEL           : std_logic;  -- MUX-B Sel
	signal ALU_OUTREG_EN      : std_logic;  -- ALU Output Register Enable
    	signal J_CODE             : std_logic_vector(2 downto 0);
	signal ALU_OPCODE         : std_logic_vector(EXE_CONTR_SIZE-1 downto 0);
	signal COMP_OPCODE        : std_logic_vector(2 downto 0);
	signal DRAM_EN            : std_logic;  -- Data RAM Write Enable
	signal DRAM_RD_WRN        : std_logic;  -- Data RAM Write Enable
	signal LMD_LATCH_EN       : std_logic;  -- LMD Register Latch Enable
	signal PC_LATCH_EN        : std_logic;  -- Program Counte Latch Enable
	signal DMEM_CW		  : std_logic_vector(DMEM_CW_SIZE-1 downto 0); -- dmemCW vector
	signal WB_MUX_SEL         : std_logic;  -- Write Back MUX Sel
	signal RF_WE_1            : std_logic;  -- Register File Write Enable 1
	signal RF_WE_2            : std_logic;  -- Register File Write Enable 2
	signal FW_EXE_MEMN_A      : std_logic;
	signal FW_EXE_MEMN_B	  : std_logic;
	signal FW_EN_A	          : std_logic;
	signal FW_EN_B	          : std_logic;
	signal FW_EN_M	          : std_logic;
	signal hold_en	          : std_logic;
	signal hold_sel	          : std_logic;
	signal FLUSH_EN	          : std_logic;


	signal IR_OUT: std_logic_vector(I_size-1 downto 0) := NOP_INST_32;

	signal sign:	std_logic;	
begin
	
		dp: datapath generic map (I_SIZE, DMEM_CW_SIZE)
		port map (
			clk => clk, 
			rst => rst, 
			sign => sign,
			IR_OUT => IR_OUT,
			IR_LATCH_EN => IR_LATCH_EN, 
			NPC_LATCH_EN => NPC_LATCH_EN,
			RegA_LATCH_EN => RegA_LATCH_EN,
			RegB_LATCH_EN => RegB_LATCH_EN, 
			RegIMM_LATCH_EN => RegIMM_LATCH_EN,
			--MUXA_SEL => MUXA_SEL, 
			MUXB_SEL => MUXB_SEL, 
			ALU_OUTREG_EN => ALU_OUTREG_EN,
			J_CODE => J_CODE,
			ALU_OPCODE => ALU_OPCODE,
			LMD_LATCH_EN => LMD_LATCH_EN,
			DMEM_CW	=> DMEM_CW,
			WB_MUX_SEL => WB_MUX_SEL, 
			RF_WE_1 => RF_WE_1,
			RF_WE_2 => RF_WE_2, 
			DRAM_EN => DRAM_EN, 
			DRAM_RD_WRN => DRAM_RD_WRN,
			FW_EXE_MEMN_A => FW_EXE_MEMN_A,
			FW_EXE_MEMN_B => FW_EXE_MEMN_B,
			FW_EN_A => FW_EN_A,
			FW_EN_B => FW_EN_B,
			FW_EN_M => FW_EN_M,
			hold_sel => hold_sel,
			hold_en => hold_en,
			FLUSH_EN => FLUSH_EN,
			--syn (imem)
			addr_imem => addr_imem,
			d_out_imem => d_out_imem,
			--syn (dmem)
			rm_dmem => rm_dmem,
			wm_dmem => wm_dmem,
			en_dmem => en_dmem,
			addr_dmem => addr_dmem,
			d_in_dmem => d_in_dmem,
			dmemCW_dmem => dmemCW_dmem,
			d_out_dmem => d_out_dmem
		);
		
	
		cu: dlx_cu generic map (
			MICROCODE_MEM_SIZE => MICROCODE_MEM_SIZE,
			FUNC_SIZE => FUNC_SIZE,
			OP_CODE_SIZE => OP_CODE_SIZE,
			ALU_OPC_SIZE => EXE_CONTR_SIZE,   
			IR_SIZE => I_SIZE,
			CW_SIZE => CW_SIZE,
			DMEM_CW_SIZE => DMEM_CW_SIZE,
			FORWARDING_ENABLED => FORWARDING_ENABLED,
			BRANCH_DELAY_ENABLED => BRANCH_DELAY_ENABLED 
		)
		port map (
			clk => clk, 
			rst => rst,
			sign => sign,
			IR_IN => IR_OUT,
			IR_LATCH_EN => IR_LATCH_EN, 
			NPC_LATCH_EN => NPC_LATCH_EN,
			RegA_LATCH_EN => RegA_LATCH_EN,
			RegB_LATCH_EN => RegB_LATCH_EN,
			RegIMM_LATCH_EN => RegIMM_LATCH_EN,
			--MUXA_SEL => MUXA_SEL, 
			MUXB_SEL => MUXB_SEL, 
			ALU_OUTREG_EN => ALU_OUTREG_EN,
			J_CODE => J_CODE,
			ALU_OPCODE => ALU_OPCODE,
			LMD_LATCH_EN => LMD_LATCH_EN, 
			PC_LATCH_EN => PC_LATCH_EN,
			DMEM_CW	=> DMEM_CW,
			WB_MUX_SEL => WB_MUX_SEL, 
			RF_WE_1 => RF_WE_1,
			RF_WE_2 => RF_WE_2,  
			DRAM_EN => DRAM_EN, 
			DRAM_RD_WRN => DRAM_RD_WRN,
			FW_EXE_MEMN_A => FW_EXE_MEMN_A,
			FW_EXE_MEMN_B => FW_EXE_MEMN_B,
			FW_EN_A => FW_EN_A,
			FW_EN_B => FW_EN_B,
			FW_EN_M => FW_EN_M,
			hold_sel => hold_sel,
			hold_en => hold_en,
			FLUSH_EN => FLUSH_EN
		);

end Structural;
