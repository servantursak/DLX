library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity adder_gen is
	generic( N: integer := 8);
	port(	en: in std_logic;
		A: in std_logic_vector(N-1 downto 0);
		B: in std_logic_vector(N-1 downto 0);
		Z: out std_logic_vector(N-1 downto 0));
end adder_gen;

architecture bhv of adder_gen is
begin
adder_gen: process(en,A,B) is
begin	
	if en = '1' then 
		Z <= A + B;
	else 
		Z <= (others => '0');
	end if;
end process adder_gen;
end bhv;
