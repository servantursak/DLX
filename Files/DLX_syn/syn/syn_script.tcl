# Analysis
analyze -library WORK -format vhdl {../src/000-constants.vhd}
analyze -library WORK -format vhdl {../src/000-utils.vhd}

#generics
analyze -library WORK -format vhdl {../src/generics/01-iv.vhd}
analyze -library WORK -format vhdl {../src/generics/01-nd2.vhd}
analyze -library WORK -format vhdl {../src/generics/02-fa.vhd}
analyze -library WORK -format vhdl {../src/generics/03-FF.vhd}
analyze -library WORK -format vhdl {../src/generics/03-FF_ddr.vhd}
analyze -library WORK -format vhdl {../src/generics/03-mux21.vhd}
analyze -library WORK -format vhdl {../src/generics/04-mux21_gen.vhd}
analyze -library WORK -format vhdl {../src/generics/04-mux21_generic.vhd}
analyze -library WORK -format vhdl {../src/generics/04-rca_generic.vhd}
analyze -library WORK -format vhdl {../src/generics/04-reg_gen.vhd}
analyze -library WORK -format vhdl {../src/generics/04-reg_gen_ddr.vhd}
analyze -library WORK -format vhdl {../src/generics/05-generic_shifter.vhd}
analyze -library WORK -format vhdl {../src/generics/06-adder_gen.vhd}

#a-DLX.core
	#a.b-CU_HW.core
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-CU_HW.core/a.b.c-forwarding.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-CU_HW.core/a.b.c-hazard_det.vhd}

	#a.b-DATAPATH.core
		#a.b.c-ALU.core
			#a.b.c.d-pentium4adder.core
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f.g-csb.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f-Gfunc.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f-PGfunc.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e-carryGen.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e-sumg.vhd}
			#../
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-boolLogic.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder_TOP.vhd}

		#a.b.c-BOOTHMUL.core
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-BoothEnc.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Comp2.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-LLS.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Mux8x1.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Sum2.vhd}
		#../
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-adder_comp.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU_TOP.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-jump_unit.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-reg_file.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-sign_ext.vhd}
	#../
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-CU_HW.vhd}
analyze -library WORK -format vhdl {../src/a-DLX.core/a.b-datapath_TOP.vhd}
#../
analyze -library WORK -format vhdl {../src/a-DLX_TOP.vhd}

elaborate dlx -architecture Structural -library WORK -parameters "I_size=32, PC_size = 8, FORWARDING_ENABLED=1, BRANCH_DELAY_ENABLED=1"

# No constraint
compile -exact_map
write -hierarchy -format vhdl -output report/dlx_no_opt.vhdl
write -hierarchy -format verilog -output report/dlx_no_opt.v
write_sdc report/dlx_no_opt.sdc
report_timing > report/dlx_timing_no_opt.rpt
report_area > report/dlx_area_no_opt.rpt
report_power > report/dlx_power_no_opt.rpt

# With constraint
create_clock -name "clk" -period 2.0 clk
set_max_delay 1.0 -from [all_inputs] -to [all_outputs]
compile -map_effort high
write -hierarchy -format vhdl -output report/dlx_with_opt.vhdl
write -hierarchy -format verilog -output report/dlx_with_opt.v
write_sdc report/dlx_with_opt.sdc
report_timing > report/dlx_timing_with_opt.rpt
report_area > report/dlx_area_with_opt.rpt
report_power > report/dlx_power_with_opt.rpt
