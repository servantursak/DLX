library ieee;
use ieee.std_logic_1164.all;

package CONSTANTS is
   constant Nbit : integer := 32;
   constant R: integer := 3;
	constant N_MUL : integer := 34;
	constant ALU_CONTR_SIZE: integer := 3;
	constant EXE_CONTR_SIZE: integer := 5;
	constant DMEM_WORD_SIZE: integer := 8;
	constant DMEM_SIZE: integer := 512;
	constant IMEM_WORD_SIZE: integer := 8;
	
	
	constant IR_SIZE: integer:= 32;
	constant PC_SIZE: integer:= 8;
	constant CW_SIZE: integer:= 20;
	constant MICROCODE_MEM_SIZE:integer :=64;
	constant REG_SIZE: integer :=32;
	constant OP_CODE_SIZE: integer :=6;
	constant OPR_SIZE: integer :=5;
	constant IMM_SIZE: integer :=16;
	constant FUNC_SIZE: integer :=11;
	constant JUMP_SIZE: integer := 26;

	constant SYSTEM_DATA_SIZE: integer := 32;
	constant DMEM_CW_SIZE: integer := 3;

	-- INSTRUCTION SET
	 
	constant R_TYPE: 	integer range 0 to 2**OP_CODE_SIZE-1  := 0;
	constant F_TYPE: 	integer range 0 to 2**OP_CODE_SIZE-1  := 1;
	constant J_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 2;
	constant JAL_INSTR:	integer range 0 to 2**OP_CODE_SIZE-1  := 3;
	constant BEQZ_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 4;
	constant BNEZ_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 5;
	constant BFPT_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 6;
	constant BFPF_INSTR:	integer range 0 to 2**OP_CODE_SIZE-1  := 7;
	constant ADDI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 8;
	constant ADDUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 9;
	constant SUBI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 10;
	constant SUBUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 11;
	constant ANDI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 12;
	constant ORI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 13;
	constant XORI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 14;
	constant LHI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 15;
	constant RFE_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 16;
	constant TRAP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 17;
	constant JR_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 18;
	constant JALR_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 19;
	constant SLLI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 20;
	constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 21;
	constant SRLI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 22;
	constant SRAI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 23;
	constant SEQI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 24;
	constant SNEI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 25;
	constant SLTI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 26;
	constant SGTI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 27;
	constant SLEI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 28;
	constant SGEI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 29;
	--constant _INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 30;
	--constant _INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 31;
	constant LB_TYPE: 	integer range 0 to 2**OP_CODE_SIZE-1  := 32;
	constant LH_TYPE: 	integer range 0 to 2**OP_CODE_SIZE-1  := 33;
	--constant _INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 34;
	constant LW_INSTR:	integer range 0 to 2**OP_CODE_SIZE-1  := 35;
	constant LBU_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 36;
	constant LHU_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 37;
	constant LF_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 38;
	constant LD_INSTR:	integer range 0 to 2**OP_CODE_SIZE-1  := 39;
	constant SB_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 40;
	constant SH_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 41;
	--constant _INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 42;
	constant SW_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 43;
	--constant ORI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 44;
	--constant XORI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 45;
	constant SF_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 46;
	constant SD_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 47;
	--constant JR_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 48;
	--constant JALR_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 49;
	--constant SLLI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 50;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 51;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 52;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 53;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 54;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 55;
	constant ITLB_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 56;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 57;
	constant SLTUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 58;
	constant SGTUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 59;
	constant SLEUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 60;
	constant SGEUI_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 61;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 62;
	--constant NOP_INSTR: 	integer range 0 to 2**OP_CODE_SIZE-1  := 63;

	--REG_REG FUNC

	--constant _RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 0;
	--constant _RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 1;
	--constant _RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 2;
	--constant _RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 3;
	constant SLL_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 4;
	--constant _RR_FUNC : 	integer range 0 to 2**FUNC_SIZE-1   := 5;
	constant SRL_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 6;
	constant SRA_RR_FUNC:	integer range 0 to 2**FUNC_SIZE-1   := 7;
	--constant _RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 8;
	-- FREE FROM 0x08 to 0x1f		
	constant ADD_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 32;
	constant ADDU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 33;
	constant SUB_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 34;
	constant SUBU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 35;
	constant AND_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 36;
	constant OR_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 37;
	constant XOR_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 38;
	--constant JR_INSTR: 	integer range 0 to 2**FUNC_SIZE-1   := 39;
	constant SEQ_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 40;
	constant SNE_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 41;
	constant SLT_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 42;
	constant SGT_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 43;
	constant SLE_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 44;
	constant SGE_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 45;
	--constant NOP_INSTR: 	integer range 0 to 2**FUNC_SIZE-1   := 46;
	--constant NOP_INSTR: 	integer range 0 to 2**FUNC_SIZE-1   := 47;
	constant MOVI2S_RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 48;
	constant MOVS2I_RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 49;
	constant MOVF_RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 50;
	constant MOVD_RR_FUNC :	integer range 0 to 2**FUNC_SIZE-1   := 51;
	constant MOVFP2I_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 52;
	constant MOVI2FP_RR_FUNC : 	integer range 0 to 2**FUNC_SIZE-1   := 53;
	constant MOVI2T_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 54;
	constant MOVT2I_RR_FUNC:	integer range 0 to 2**FUNC_SIZE-1   := 55;
	--constant _RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 56;
	--constant _RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 57;
	constant SLTU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 58;
	constant SGTU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 59;
	constant SLEU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 60;
	constant SGEU_RR_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 61;

	--F TYPE INSTRUCTIONS
	constant MULTU_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 22;
	constant MULT_FUNC: 	integer range 0 to 2**FUNC_SIZE-1   := 14;

	-- ALU INTEGER TYPES
	constant ALU_ADD: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 0;
	constant ALU_SUB: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 1;
	constant ALU_AND: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 2;
	constant ALU_OR: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 3;
	constant ALU_XOR: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 4;
	constant ALU_SLL: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 5;
	constant ALU_SRL: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 6;
	constant ALU_SRA: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 7;

	constant MULTU: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 8;
	constant MULT: 		integer range 0 to 2**EXE_CONTR_SIZE-1   := 9;
	
	constant ALU_NOP0: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 16;
	constant ALU_SEQ: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 17;
	constant ALU_SNE: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 18;
	constant ALU_SLT: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 19;
	constant ALU_SLE: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 20;
	constant ALU_SGT: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 21;
	constant ALU_SGE: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 22;
	constant ALU_NOP1: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 23;
		
	constant ALU_NOP2: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 24;
	constant ALU_NOP3: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 25;
	constant ALU_NOP4: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 26;
	constant ALU_SLTU: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 27;
	constant ALU_SLEU: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 28;
	constant ALU_SGTU: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 29;
	constant ALU_SGEU: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 30;
	constant ALU_NOP5: 	integer range 0 to 2**EXE_CONTR_SIZE-1   := 31;
	
	-- ALU ARRAY TYPES
	constant ALU_ADD_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00000";
	constant ALU_SUB_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00001";
	constant ALU_AND_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00010";
	constant ALU_OR_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00011";
	constant ALU_XOR_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00100";
	constant ALU_SLL_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00101";
	constant ALU_SRL_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00110";
	constant ALU_SRA_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "00111";

	constant MULTU_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "01000";
	constant MULT_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "01001";

	constant ALU_NOP0_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10000";
	constant ALU_SEQ_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10001";
	constant ALU_SNE_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10010";
	constant ALU_SLT_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10011";
	constant ALU_SLE_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10100";
	constant ALU_SGT_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10101";
	constant ALU_SGE_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10110";
	constant ALU_NOP1_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "10111";
		
	constant ALU_NOP2_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11000";
	constant ALU_NOP3_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11001";
	constant ALU_NOP4_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11010";
	constant ALU_SLTU_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11011";
	constant ALU_SLEU_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11100";
	constant ALU_SGTU_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11101";
	constant ALU_SGEU_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11110";
	constant ALU_NOP5_A: 	std_logic_vector(EXE_CONTR_SIZE-1 downto 0)  := "11111";


	constant NOP_INST_32: std_logic_vector(IR_SIZE-1 downto 0) := "01010100000000000000000000000000";
end CONSTANTS;
