LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.constants.all;
 
ENTITY dlx_tb IS
END dlx_tb;
 
ARCHITECTURE behavior OF dlx_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 	component dlx is
		generic(I_size: integer := IR_SIZE;
			PC_size: integer := PC_SIZE;
			FORWARDING_ENABLED :     boolean := FALSE;
			BRANCH_DELAY_ENABLED:    boolean := FALSE);
		port(	clk: std_logic;
			rst: std_logic);
	end component dlx;

 	signal clk,rst: std_logic;
	
	constant clk_period: time := 10 ns;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dlx 
	GENERIC MAP(I_SIZE => IR_SIZE,
		    PC_SIZE => PC_SIZE,  
		    FORWARDING_ENABLED => FALSE, --> CHANGE PARAMETER HERE
		    BRANCH_DELAY_ENABLED => TRUE)
	PORT MAP (
          clk => clk,
          rst => rst
        );

   clk_proc: process
   begin
	clk <= '1';
	wait for clk_period/2;
	clk <= '0';
	wait for clk_period/2;
   end process;

  -- Stimulus process
   stim_proc: process
   begin
	rst <= '1';
	wait for clk_period*10;
	rst <= '0';
	wait for clk_period*2;
	wait;	
   end process;

END;
