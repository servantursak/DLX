LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.constants.all;
use std.textio.all;
use ieee.std_logic_textio.all;
 
ENTITY forwarding_tb IS
END forwarding_tb;
 
ARCHITECTURE behavior OF forwarding_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 	component forwarding is 
		generic(I_size: integer := IR_SIZE);
		port(	clk,rst: in std_logic;
			IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
			stall: in std_logic;
			EXE_MEMN_A: out std_logic;
			EXE_MEMN_B: out std_logic;
			FW_EN_A: out std_logic;
			FW_EN_B: out std_logic;
			FW_EN_M: out std_logic
		);
	end component;


 	signal nclk,clk,rst: std_logic := '0';
	signal IR_IN: std_logic_vector(IR_SIZE-1 downto 0):= (others => '0');
	signal stall, FW_EN_A, FW_EN_B, FW_EN_M, EXE_MEMN_A, EXE_MEMN_B: std_logic := '0';
	
	constant clk_period: time := 10 ns;
BEGIN
 
	nclk <= not(clk);

	-- Instantiate the Unit Under Test (UUT)
   uut: forwarding		 
	GENERIC MAP(IR_SIZE)
	PORT MAP (
          clk => nclk,
          rst => rst,
	  IR_IN => IR_IN,
	  EXE_MEMN_A => EXE_MEMN_A,
	  EXE_MEMN_B => EXE_MEMN_B,
	  FW_EN_A => FW_EN_A,
	  FW_EN_B => FW_EN_B,
	  FW_EN_M => FW_EN_M,
	  stall => stall
        );

   clk_proc: process
   begin
	clk <= '1';
	wait for clk_period/2;
	clk <= '0';
	wait for clk_period/2;
   end process;

  -- Stimulus process
   stim_proc: process
	file mem_fp: text;
	constant filename: string :="../forwarding.txt"; --can be changed
	variable file_line : line;
	variable i : integer := 0;
	variable temp : std_logic_vector(IR_size-1 downto 0);
   begin
	rst <= '1';
	wait for clk_period*10;
	rst <= '0';
	wait for clk_period*2;


	file_open(mem_fp,filename,READ_MODE);
      	while (not endfile(mem_fp)) loop
        	readline(mem_fp,file_line);
        	hread(file_line,temp);	

		IR_IN <= temp;
		wait for clk_period;
	end loop;
	
	wait;	
   end process;

END;
