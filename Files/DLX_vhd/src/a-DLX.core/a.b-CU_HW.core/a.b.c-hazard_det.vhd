library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;

entity hazard_det is
	generic(I_size: integer := IR_SIZE;
		FORWARDING_ENABLED :     boolean := FALSE;
		BRANCH_DELAY_ENABLED:    boolean := FALSE);
	port(	clk,rst: in std_logic;
		IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
		stall: out std_logic;
		hold_sel: out std_logic;
		hold_en: out std_logic);
end hazard_det;

architecture Behavioral of hazard_det is
	subtype reg_st is std_logic_vector(OPR_SIZE-1 downto 0); 
	type reg_t is array (0 to 2) of reg_st;	
	signal regD: reg_t;
	
	signal regSa, regSb: reg_st;
	
	--signal regD_tmp,regSa_tmp,regSb_tmp: reg_st;

	subtype opc_st is std_logic_vector(OP_CODE_SIZE-1 downto 0); 
	type opc_t is array (0 to 2) of opc_st;	
	signal opcode: opc_t;
	
	--signal opcode_tmp: opc_st;
	
	signal is_stalled, counting_end: std_logic;
	signal hold_sel_sig, hold_en_sig: std_logic;
	signal counter, th_value: integer range 0 to 3;

begin
	IR_IN_read_Proc: process(IR_IN,rst,clk,is_stalled)
	begin
		if rst = '1' then 
			regD <= (others => (others => '0'));
			opcode <= (others => (others => '0'));
			regSa <= (others => '0' );
			regSb <= (others => '0' );
		else
			-- WHEN IR_IN CHANGE
			if is_stalled = '0' and not clk'event then
				opcode(0) <= IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE);		
				case IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE+1) is 
					when "00000" => 
						regSa <= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);
						regSb <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);
						regD(0) <= IR_IN(IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-3*OPR_SIZE); 
					
					when others => 
						regSa<= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);
						regSb<= (others => '0');
						regD(0) <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);
				end case;
				if IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE) = "000011" or
				   IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE) = "010011" then
				   -- JUMP AND LINK EXCEPTION
				   regD(0) <= "11111";
				end if;

				if IR_IN(IR_SIZE-1 downto IR_SIZE-3) = "101" then
				   -- STORE EXCEPTION (2 source reg, no dest reg)
				   regSa <= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);		   regSb <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);  
				   regD(0) <= (others => '0');
				end if;

				for i in 1 to 2 loop
					regD(i) <= regD(i-1);
					opcode(i) <= opcode(i-1);
				end loop;
			elsif is_stalled = '1' then
			-- IN CASE OF STALL
			-- INSERT A BUBBLE
				regSa <= (others => '0'); 
				regSb <= (others => '0');
				regD(0) <= (others => '0');
				opcode(0) <= (others => '0');
				-- SHIFT THE REGS
				if clk'event  and clk = '1' then
					for i in 1 to 2 loop
						regD(i) <= regD(i-1);
						opcode(i) <= opcode(i-1);
					end loop;
				end if;
			end if;
		end if;
	
	end process IR_IN_read_Proc;
	
	syncProc: process(clk,rst)
	begin
		if rst = '1' then
			counter <= 0;
		elsif clk = '1' and clk'event  then
			if is_stalled = '1' then
				counter <= counter + 1;
			else
				counter <= 0;
			end if;
		end if;
	end process syncProc;

	counterCheckProc: process(counter)
	begin
		if rst = '1' then
			counting_end <= '0';
		elsif is_stalled = '1' and counter = th_value  then
			counting_end <= '1';
		end if;

		if th_value = 0 then
			counting_end <= '0';
		end if;		
	end process counterCheckProc;

	detectProc:process(counting_end,rst,regSa,regSb,regD)
	begin
		if rst = '1' then 		
			is_stalled <= '0';
			th_value <= 0;	
		else--if is_stalled = '0' then
			if FORWARDING_ENABLED = true then
				-- if forwarding is enabled, only load instructions could
				-- cause data hazards
		
				-- check for load instr.
				if opcode(1)(OP_CODE_SIZE-1 downto 3) = "100" and regD(1) /= "00000" then
					if (regD(1) = regSa) or 
						(opcode(0)(OP_CODE_SIZE-1 downto 3) /= "101" and (regD(1) = regSb) ) then
						-- for load instr Sb is used in mem stage => no hazard with forwarding
						is_stalled <= '1';  
						th_value <= 1;
				
					end if;
				end if;
			else 
				-- if forwarding is disabled, an instruction reading a reg 
				-- written by another one instr. should wait until it's finished

				
				if regD(1) /= "00000" and (regD(1) = regSa or regD(1) = regSb) then
				-- DATA DEP ON INSTR - 1				
					is_stalled <= '1';
					th_value <= 2;
				elsif regD(2) /= "00000" and ( regD(2) = regSa or regD(2) = regSb ) then				-- DATA DEP ON INSTR - 2
					is_stalled <= '1';
					th_value <= 1;
				end if;
		

			
			end if;

			if  BRANCH_DELAY_ENABLED = false then
			-- Disable stall for instruction inside branch delay slot 
			-- (it could be never executed)
				if opcode(1) = "000010" or opcode(1) = "000011" or 
				   opcode(1) = "000100" or opcode(1) = "000101" or
	            	           opcode(1) = "010010" or opcode(1) = "010011" then
					is_stalled <= '0';  
					th_value <= 0;
				end if;
			end if;
	
		end if;
							 
		if counting_end = '1' then
			is_stalled <= '0';
			th_value <= 0;
		end if;
		
	end process detectProc;

	holdProc: process(is_stalled,rst,counter)
	begin
		if rst = '1' then
			hold_sel_sig <= '0';
			hold_en_sig <= '0';		
		elsif is_stalled = '1' and hold_sel_sig = '0' then
		-- Check for branch instr. on prev instr.
			if opcode(1) = "000010" or opcode(1) = "000011" or 
			 opcode(1) = "000100" or opcode(1) = "000101" or
	            	 opcode(1) = "010010" or opcode(1) = "010011" then
				hold_sel_sig <= '1';
				hold_en_sig <= '1';
			end if;
		end if;

		if hold_en_sig = '1' then
			hold_en_sig <= '0';		
		end if;
	
		if hold_sel_sig = '1'and counter =  0 then
			hold_sel_sig <= '0';
		end if;
	end process holdProc;


	-- OUTPUTS
	hold_en <= hold_en_sig;
	hold_sel <= hold_sel_sig;
	stall <= is_stalled;
end Behavioral;
