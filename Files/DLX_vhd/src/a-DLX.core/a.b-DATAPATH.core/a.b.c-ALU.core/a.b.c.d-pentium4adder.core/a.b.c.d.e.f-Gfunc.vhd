library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Gfunc is
	port(
		gik, pik: in std_logic;
		gikp: in std_logic;
		gij: out std_logic
	);
end Gfunc;

architecture Behavioral of Gfunc is

begin

	gij <= gik or (pik and gikp);

end Behavioral;

