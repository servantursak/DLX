library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.constants.all;

entity imem is
	generic(PC_size: integer := 8;
		IR_size: integer := 32);
	port(	reset: in std_logic;
		addr: in std_logic_vector(PC_size-1 downto 0);
		d_out: out std_logic_vector(IR_size-1 downto 0));
end imem;

architecture bhv of imem is

type memtype is array (0 to 2**PC_size - 1) of std_logic_vector(IMEM_WORD_SIZE-1 downto 0);
signal i_mem : memtype;

begin

d_out <= std_logic_vector(i_mem(to_integer(unsigned(addr))))&std_logic_vector(i_mem(to_integer(unsigned(addr))+1))&std_logic_vector(i_mem(to_integer(unsigned(addr))+2))&std_logic_vector(i_mem(to_integer(unsigned(addr))+3)) when reset = '0' else NOP_INST_32;

FILL_MEM_P: process(reset)
    file mem_fp: text;
    constant filename: string :="../imem.txt"; --can be changed
    variable file_line : line;
    variable i : integer := 0;
    variable temp : std_logic_vector(IR_size-1 downto 0);
begin 
    if (reset = '1') then
      file_open(mem_fp,filename,READ_MODE);
      while (not endfile(mem_fp)) loop
        readline(mem_fp,file_line);
		--  next when file_line(1)='#';
        hread(file_line,temp);
        i_mem(i) <= temp(NBIT-1 downto NBIT-IMEM_WORD_SIZE);
        i_mem(i+1) <= temp(NBIT-IMEM_WORD_SIZE-1 downto NBIT-2*IMEM_WORD_SIZE);
        i_mem(i+2) <= temp(NBIT-2*IMEM_WORD_SIZE-1 downto NBIT-3*IMEM_WORD_SIZE);
        i_mem(i+3) <= temp(NBIT-3*IMEM_WORD_SIZE-1 downto NBIT-4*IMEM_WORD_SIZE);     
        i := i + 4;
      end loop;
      -- FILL THE OTHER ROWS WITH NOP OPS
      while i < 2**PC_SIZE loop 
	i_mem(i) <= NOP_INST_32(NBIT-1 downto NBIT-IMEM_WORD_SIZE);
        i_mem(i+1) <= NOP_INST_32(NBIT-IMEM_WORD_SIZE-1 downto NBIT-2*IMEM_WORD_SIZE);
        i_mem(i+2) <= NOP_INST_32(NBIT-2*IMEM_WORD_SIZE-1 downto NBIT-3*IMEM_WORD_SIZE);
        i_mem(i+3) <= NOP_INST_32(NBIT-3*IMEM_WORD_SIZE-1 downto NBIT-4*IMEM_WORD_SIZE);     
        i := i + 4;
      end loop; 
    end if;
end process FILL_MEM_P;

end bhv;
