-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;
  USE WORK.CONSTANTS.ALL;
  
  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 
	component ALU is
		generic (
			N: integer := Nbit;
			ALU_CONTR_SIZE: integer:= ALU_CONTR_SIZE
		);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			contr: in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
			S: out std_logic_vector(N-1 downto 0)
		);
	end component ALU;

	signal A,B,S: std_logic_vector(N-1 downto 0):= (others => '0');
	signal contr: std_logic_vector(ALU_CONTR_SIZE-1 downto 0):= (others => '0');
   signal opA, opB: integer :=0;
          

  BEGIN

  -- Component Instantiation
       
	uut: ALU
		generic map(N,ALU_CONTR_SIZE)
		port map(A,B,contr,S);

  --  Test Bench Statements
     tb : PROCESS
     BEGIN
		  wait for 10 ns;
		  A <= std_logic_vector(to_unsigned(12512512, N));
		  B <= std_logic_vector(to_unsigned(56437232, N));		  
		  for i in 0 to 7 loop
				contr <= std_logic_vector(to_unsigned(i,ALU_CONTR_SIZE));
		  		wait for 10 ns;				
		  end loop;
        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
