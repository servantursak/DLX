LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY p4tb IS
END p4tb;
 
ARCHITECTURE behavior OF p4tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT pentium4adder
	 generic(	N: integer := 16);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		Ci: in std_logic;
		S: out std_logic_vector(N-1 downto 0);
		Co: out std_logic
	);
    END COMPONENT;

	component carryGen
	generic (N: integer := 32; R: integer := 4);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			Ci: in std_logic;
			Co_0: out std_logic;
			C: out std_logic_vector(N/R -1 downto 0)
		);
	end component;
    

	constant N : integer := 32;
	constant R : integer := 4;
   --Inputs
   signal A : std_logic_vector(N-1 downto 0) := (others => '0');
   signal B : std_logic_vector(N-1 downto 0) := (others => '0');
   signal Ci : std_logic := '0';

	--Intermediate signals	
	--signal Co_all: std_logic_vector(N/4-1 downto 0);
 	
	--Outputs
	signal S_P4 : std_logic_vector(N-1 downto 0);
	signal Co_P4: std_logic;
	--signal Co_0_cg: std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pentium4adder 
	GENERIC MAP(N)
	PORT MAP (
          A => A,
          B => B,
          Ci => Ci,
          S => S_P4,
          Co => Co_P4
        );

 	
	--dut: carryGen
		--generic map(N, R)
		--port map( A, B, Ci, Co_0_cg, Co_all);


   -- Stimulus process
   stim_proc: process
   begin
	
	A <= "11111111111111111111111111111111";
	B <= "00000000000000000000000000000001";
	Ci <= '0';
	
	wait for 1 ns;
	A <= "11001100110011001100110011001100";
	B <= "00110011001100110011001100110011";
	Ci <= '0';	

	wait for 1 ns;
	A <= "11001100110011001100110011001100";
	B <= "00110011001100110011001100110011";
	Ci <= '1';

	wait for 1 ns;
	A <= "00110011001100110011001100110011";
	B <= "11001100110011001100110011001100";
	Ci <= '0';

	wait for 1 ns;
	A <= "00110011001100110011001100110011";
	B <= "11001100110011001100110011001100";
	Ci <= '1';

	wait for 1 ns;
	A <= "00000000000000000000000000000001";
	B <= "11111111111111111111111111111111";
	Ci <= '1';

	wait for 1 ns;
	A <= "01101010100011011000111001001111";
	B <= "11010010001101010001101100000000";
	Ci <= '1';

	wait for 1 ns;
	A <= "01101010100011011000111001001111";
	B <= "11010010001101010001101100000000";
	Ci <= '0';

	wait for 1 ns;			
end process;

END;
