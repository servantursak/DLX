addi r2, r0, -15
addi r1, r0, 15
label:
seqi r3, r1, 0 # 0-0
seqi r4, r1, 15 # 1-0
seqi r5, r1, -15 # 0-1
snei r6, r1, 0 # 1-1
snei r7, r1, 15 # 0-1
snei r8, r1, -15 # 1-0
sgti r9, r1, 0 # 1-0
sgti r10, r1, 15 # 0-0
sgti r11, r1, -15 # 1-0
sgei r12, r1, 0 # 1-0
sgei r13, r1, 15 # 1-0
sgei r14, r1, -15 # 1-1
slti r15, r1, 0 # 0-1
slti r16, r1, 15 # 0-1
slti r17, r1, -15 # 0-0
slei r18, r1, 0 # 0-1
slei r19, r1, 15 # 1-1
slei r20, r1, -15 # 0-1
sltui r21, r1, 0 # 0-0
sltui r22, r1, 15 # 0-0
sltui r23, r1, -15 # 1-0
sleui r24, r1, 0 # 0-0
sleui r25, r1, 15 # 1-0
sleui r26, r1, -15 # 1-1
sgtui r27, r1, 0 #1-1
sgtui r28, r1, 15 #0-1
sgtui r29, r1, -15 #0-0
sgeui r30, r1, 0 # 1-1
sgeui r31, r1, 15 # 1-1
sgeui r31, r1, -15 # 0-1
seq r3, r1, r0 # 0-0
seq r4, r1, r1 # 1-1
seq r5, r1, r2 # 0-1
sne r6, r1, r0 # 1-1
sne r7, r1, r1 # 0-0
sne r8, r1, r2 # 1-0
sgt r9, r1, r0 # 1-0
sgt r10, r1, r1 # 0-0
sgt r11, r1, r2 # 1-0
sge r12, r1, r0 # 1-0
sge r13, r1, r1 # 1-1
sge r14, r1, r2 # 1-1
slt r15, r1, r0 # 0-1
slt r16, r1, r1 # 0-0
slt r17, r1, r2 # 0-0
sle r18, r1, r0 # 0-1
sle r19, r1, r1 # 1-1
sle r20, r1, r2 # 0-1
sltu r21, r1, r0 # 0-0
sltu r22, r1, r1 # 0-0
sltu r23, r1, r2 # 1-0
sleu r24, r1, r0 # 0-0
sleu r25, r1, r1 # 1-1
sleu r26, r1, r2 # 1-1
sgtu r27, r1, r0 # 1-1
sgtu r28, r1, r1 # 0-0
sgtu r29, r1, r2 # 0-0
sgeu r30, r1, r0 # 1-1 
sgeu r31, r1, r1 # 1-1 
sgeu r31, r1, r2 # 0-1 
add r1, r0, r2
beqz r28, label
