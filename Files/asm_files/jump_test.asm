addui r1, r0, 1
j label1
nop
addi r1, r0, -1		# if a negative value is written => branch failed
			# check R1, should be 1
label1:
bnez r2, label2
nop
addi r2, r0, 2
bnez r2, label2
nop
addui r2, r0, -2	# if a negative value is written => branch failed
			# check R2, should be 2
label2:
beqz r1, label3
nop
addi r3, r0, 3
beqz r4, label3
nop
addui r3, r0, -3	# if a negative value is written => branch failed
			# check R3, should be 3
label3:
addi r4, r0, 4
jal label4
nop
addi r4, r0, -4
			# check R4, should be 4
			# check R31, should be written
label4:
addi r31, r31, 28
jr 31
nop
addi r5, r0, -5
			# check R5, should be 0
label5:
jalr r0
nop
			# check R31, should be changed
			# now the test should be restarted, otherwise it has failed

