addi r1, r0, 1
subi r2, r0, -2
addui r3, r0, 3
subui r4, r0, 4
andi r5, r1, 3
ori r6, r1, 0
xori r7, r1, 2
slli r8, r1, 15
srli r9, r4, 3
srai r10, r4, 5
seqi r11, r1, -1 #	0
snei r12, r1, 0	 #	1
sgti r13, r1, 0	 #	1
sgei r14, r1, -2 #	1
slti r15, r1, 5	 #	1
slei r16, r1, -1 #	0
sltui r17, r1, 1 #	0
sleui r18, r1, 0 #	0
sgtui r19, r1, 2 #	0	
sgeui r20, r1, 0 #	1
# reg-reg type
sll r21, r1, r2
srl r22, r4, r3
sra r23, r4, r7
add r24, r1, r4
addu r25, r1, r4
sub r26, r1, r2
subu r27, r2, r1
and r28, r4, r1
or r29, r1, r2
xor r30, r1, r3
seq r31, r1, r1	#	1		
sne r31, r1, r2 #	1
slt r31, r1, r2	#	1
sgt r31, r1, r2	#	0
sle r31, r1, r2	#	1
sge r31, r1, r2	#	0
sltu r31, r1, r2 #	1
sgtu r31, r1, r2 #	0
sleu r31, r1, r2 #	1
sgeu r31, r1, r2 #	0
j label1
nop
addi r1, r0, 2

# test jumps

label1:
bnez r1, label1
subi r1, r1, 1 # exploiting branch delay slot

label2:
beqz r1, label2
addi r1, r1, 1

jal labeljal
nop
nop
addui r1, r31, 16
jalr r1 
nop
nop
j endLabel
nop

labeljal:
nop
addi r1, r0, 252
jr r31
nop

labeljalr:
jr r10
nop

endLabel:
