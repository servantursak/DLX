library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity add4 is
	generic( PC_size: integer := 8);
	port(	A: in std_logic_vector(PC_size-1 downto 0);
		B: in std_logic_vector(PC_size-1 downto 0);
		Z: out std_logic_vector(PC_size-1 downto 0));
end add4;

architecture bhv of add4 is
begin
add4: process(A,B) is
begin	
	Z <= A + B;
end process add4;
end bhv;
