library IEEE;
use IEEE.std_logic_1164.all;

entity mux21_gen is	
	Generic (NBIT: integer:= 32);
	Port (	A:	In	std_logic_vector(NBIT-1 downto 0) ;
		B:	In	std_logic_vector(NBIT-1 downto 0);
		SEL:	In	std_logic;
		Y:	Out	std_logic_vector(NBIT-1 downto 0));
end mux21_gen;

architecture BEHAVIORAL of mux21_gen is

begin
	Y <= A when SEL='0' else B;

end BEHAVIORAL;









