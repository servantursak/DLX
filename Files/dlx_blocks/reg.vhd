library IEEE;
use IEEE.std_logic_1164.all; 

entity reg is
	Port (	EN:	In	std_logic;
		D:	In	std_logic;
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic);
end reg_latch;


architecture sync of reg is
begin
	PLATCH: process(CK,RESET)
	begin
	if RESET='1' then
	 	Q <= '0';
	elsif clk'event and CK='1' then
		if(EN='1') then
	 		Q <= D; 
		end if;
	end if;

	end process;
end sync;
