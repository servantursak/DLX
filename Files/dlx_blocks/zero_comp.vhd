library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity zero_comp is
	generic(reg_size: integer := 32);
	port(	A: in std_logic_vector(reg_size-1 downto 0);
		Z: out std_logic);
end zero_comp;

architecture bhv of zero_comp is

signal temp: std_logic_vector(reg_size-1 downto 0);
signal zeroORnot: std_logic;
signal all_zeros: std_logic_vector(reg_size-1 downto 0) := (others => '0');
signal all_ones: std_logic_vector(reg_size-1 downto 0) := (others => '1');

begin

temp <= A xnor all_zeros;
zeroORnot <= '1' when temp=all_ones else '0';
Z <= zeroORnot;

end bhv;


